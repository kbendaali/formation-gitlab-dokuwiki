# Notes générales
# ===============
# * le "cd X" tout seul ne sert à rien (car pas d'impact sur l'image) on le
#   relie donc aux commandes concernées par le changement de dossier
# * On pense à passer les en non-interactif (ici avec le
#   parametre "-y" pour apt)
# * On pense à récupérer les catalogues avant de télécharger des packages
# * On pense à supprimer les paquets zippés en cache apres l'installation (ici
#   avec "apt-get clean")
# * On veut un environnement totalement maitrisable et reproductible, donc on
#   évite tout ce qui fait des suggestions ou des modifications automatique
#   et non-matrisées (ex: option --no-install-recommends  --no-install-suggests
#   pour apt)
# * On supprime tout ce qui est mis en cache et qui risque d'être obsolete
#   rapidement (ex: les catalogues apt dans /var/lib/apt/lists/...)
# * On supprime tous les fichiers temporaires inutiles (ex: /tmp/ /var/tmp)
# * On vide tous les fichiers de logs (car docker n'utilise pas le systeme de
#   gestion des logs classique de unix rsyslog)
# * On fait en sorte de maitriser les versions des trucs qu'on installe
#   et pas faire confiance aux serveurs tiers qui peuvent changer une référence
#   sans qu'on le voie (ex: que veut dire version "stable" => préciser numéro)

# On utilise une image de base (obligatoire)
FROM debian:11
MAINTAINER Glenn Y. Rolland <teaching@glenux.net>

# Installation des packages apache et php7.4
RUN apt-get update \
 && apt-get install \
 		-qq \
        --no-install-recommends --no-install-suggests --yes \
        apache2 php7.4 wget \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 && truncate -s 0 /var/log/*log

# Téléchargement de dokuwiki (attention, leur certificat TLS semble foireux)
RUN wget --no-check-certificate --quiet -O /usr/src/dokuwiki.tgz \
      https://download.dokuwiki.org/src/dokuwiki/dokuwiki-2020-07-29.tgz \
 && cd /usr/src \
 && tar xaf dokuwiki.tgz \
 && rm -fr /var/www/html \
 && mv dokuwiki-2020-07-29 /var/www/html \
 && chown -R www-data:www-data /var/www/html \
 && rm -f /usr/src/dokuwiki.tgz

# sur les linux anciens & autres unix : /etc/init.d/XXX start
# sur les linux récents : systemctl start XXX
# CMD systemctl start apache2 => pas de systemctl dans docker
# CMD /etc/init.d/apache2 start => les processus se détachent, aïe...
CMD apache2ctl -D FOREGROUND

